function res = QIF_FRE_and_adjoint(t,y,Delta,J,Theta,Tm,Td)
% QIF-FRE with exponential synapses and adjoint eq.

R = y(1); V = y(2); S = y(3);
n1 = y(4); n2 = y(5); n3 = y(6);

dR = (1/Tm)*(Delta/(pi*Tm) + 2*R*V);
dV = (1/Tm)*(V^2 - (pi*Tm*R)^2 - J*Tm*S + Theta);
dS = (1/Td)*(R - S);

dn1 = -2*V*(1/Tm)*n1 + 2*pi^2*R*Tm*n2 - (1/Td)*n3;
dn2 = -2*R*(1/Tm)*n1 - 2*V*(1/Tm)*n2;
dn3 = J*n2 + (1/Td)*n3;

res = [dR; dV; dS; dn1; dn2; dn3];

end