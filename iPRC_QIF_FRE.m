function [t,iPRC,V] = iPRC_QIF_FRE(eps,y0,T,Delta,J,Theta,Tm,Td)
%%% Input %%%
% eps: perturbation vector.
% y0: Initial condition [R0;V0;S0] *ON THE LIMIT CYCLE*.
% T: period of the periodic orbit.
% Delta,J,Theta,Tm,Td: Parameters of the QIF-FRE system (Eq. (3.13)).
%%% Output %%%
% t: Integration time vector. It should be in [0,T].
% iPRC: vector with the iPRC at each value of t.
% V: Membrane potential at each value of t, for reference.

options = odeset('RelTol',1e-12,'AbsTol',1e-12);

f = @(t,y) QIF_FRE(t,y,Delta,J,Theta,Tm,Td); % ODE vector field
F = @(t,y) QIF_FRE_and_adjoint(t,y,Delta,J,Theta,Tm,Td); % VF + adjoint eq.

% Backward integration to solve Adjoint eq.
t0 = 0;
y0 = [y0; f(0,y0)];
for k = 1:4 % 4 cycles is enough to "return" to the periodic solution of the adjoint equation. Increase it if necessary.
    [t,y] = ode45(F,[t0,-T],y0,options);
    y0 = [y0(1:3);y(end,4:end)'];
end
z = y(:,4:end)/(y(1,4:end)*f(0,y(1,1:3)));
% Remove this normalization to obtain the same $[\nabla\Theta]_V$ as in
% Figure 3.10

% Computation of the iPRC
iPRC = zeros(1,length(t));
for i = 1:length(t)
    iPRC(i) = z(i,:)*eps;
end

t = t-t(end);

V = flip(y(:,2));
t = flip(t);
iPRC = flip(iPRC);

end

