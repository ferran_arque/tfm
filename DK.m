function [dKx,dKy,dKz] = DK(theta,sig1,sig2)
%%% Input %%%
% theta, sig1, sig2: Phase and amplitude coordinates (scalars)
%%% Output %%%
% dKx, dKy, dKz: 1st, 2nd and 3rd rows of the matrix DK (differential of K) resp. 

vTheta = linspace(0,1,2048+1);
h = vTheta(2)-vTheta(1);
iTheta = find(abs(vTheta-theta)<h/2); % Index of closest value to theta in linspace(0,1,2048+1)

Kxij = dlmread('kx.dat');
Kyij = dlmread('ky.dat');
Kzij = dlmread('kz.dat');
% Centered differences to approximate 1st derivative
if iTheta == 1 || iTheta == 2049 
    dKxij = (Kxij(2,:)-Kxij(2048,:))/(2*h);
    dKyij = (Kyij(2,:)-Kyij(2048,:))/(2*h);
    dKzij = (Kzij(2,:)-Kzij(2048,:))/(2*h);
    
    Kxij = Kxij(1,:);
    Kyij = Kyij(1,:);
    Kzij = Kzij(1,:);
else
    dKxij = (Kxij(iTheta+1,:)-Kxij(iTheta-1,:))/(2*h);
    dKyij = (Kyij(iTheta+1,:)-Kyij(iTheta-1,:))/(2*h);
    dKzij = (Kzij(iTheta+1,:)-Kzij(iTheta-1,:))/(2*h);
    
    Kxij = Kxij(iTheta,:);
    Kyij = Kyij(iTheta,:);
    Kzij = Kzij(iTheta,:);
end


L = 10;
dKx = [dKxij(1),0,0]; dKy = [dKyij(1),0,0]; dKz = [dKzij(1),0,0];
for m = 1:L
    ind = m*(m+1)/2;
    for a = 0:m
        dKx = dKx + [dKxij(ind+a+1)*sig1^(m-a)*sig2^a, mult(m-a,Kxij(ind+a+1)*sig1^(m-a-1)*sig2^a), mult(a,Kxij(ind+a+1)*sig1^(m-a)*sig2^(a-1))];
        dKy = dKy + [dKyij(ind+a+1)*sig1^(m-a)*sig2^a, mult(m-a,Kyij(ind+a+1)*sig1^(m-a-1)*sig2^a), mult(a,Kyij(ind+a+1)*sig1^(m-a)*sig2^(a-1))];
        dKz = dKz + [dKzij(ind+a+1)*sig1^(m-a)*sig2^a, mult(m-a,Kzij(ind+a+1)*sig1^(m-a-1)*sig2^a), mult(a,Kzij(ind+a+1)*sig1^(m-a)*sig2^(a-1))];
    end
end

function res = mult(a,b)
    % To avoid 0*inf = NaN. In this case it should be 0, given how we wrote
    % the derivatives in general
	if a == 0 || b == 0, res = 0;
    else, res = a*b; end
end

end