function L = S_theta(theta,sig_max,Dmax)
% Leaf S_theta of the slow manifold (see Algorithm 3.5 
% in Perez-Cervera2020) given by a matrix L with 3
% columns, one for each coordinate. Note that this is
% for a specific case, so the period T, the eigenvalue
% lambda2, etc. are already given.

options = odeset('RelTol',1e-12,'AbsTol',1e-12);

Delta = 0.3;
J = 21;
Theta = 4;
Tm = 10;
Td = 5;

T = 27.579110226927583;
lambda2 = -0.059922982488808;

[kxx,kyy,kzz] = K(theta,0,0);
x0 = [kxx,kyy,kzz]; xk = x0;
L = x0;

k = 0;
n = 0;
sig = 0;
Dsig = 0.8*sig_max;

while xk(1)<0.2 && xk(2)<3 && xk(2)>-4 && xk(3)<0.1
    while abs(sig + Dsig) < abs(sig_max)
        [kxx,kyy,kzz] = K(theta,0,sig+Dsig);
        x_int = [kxx,kyy,kzz];
        if n == 0, xk = x_int;
        else
            [~,y] = ode45(@(t,y) QIF_FRE(t,y,Delta,J,Theta,Tm,Td),[0,-n*T],x_int,options);
            xk = y(end,:);
        end
        if norm(xk-x0) < Dmax
            L = [L;xk];
            sig = sig + Dsig;
            k = k + 1;
            x0 = xk;
        else
            Dsig = Dsig/2;
        end
    end
    sig = sig_max*exp(lambda2*T);
    Dsig = 0.8*(sig_max-sig);
    n = n + 1;
end

end