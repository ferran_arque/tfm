function res = E(theta,sig1,sig2,T,lambdas,Delta,J,Theta,Tm,Td)
%%% Input %%%
% theta, sig1, sig2: Phase and amplitude coordinates (scalars).
% T: Period of the periodic orbit.
% lambdas: Vector with the nontrivial characteristic exponents.
% Delta, J, Theta, Tm, Td: Parameters of the N-QIF system.
%%% Output %%%
% res: Error function E. Eq. (48) in Perez-Cervera2020.

[dKx,dKy,dKz] = DK(theta,sig1,sig2);
dKtheta = [dKx(1),dKy(1),dKz(1)];
dKs1 = [dKx(2),dKy(2),dKz(2)];
dKs2 = [dKx(3),dKy(3),dKz(3)];
[Kx,Ky,Kz] = K(theta,sig1,sig2);
XK = QIF_FRE(0,[Kx,Ky,Kz],Delta,J,Theta,Tm,Td);

res = (1/T)*dKtheta + lambdas(1)*sig1*dKs1 + lambdas(2)*sig2*dKs2 - XK';

end