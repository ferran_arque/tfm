function res = QIF_FRE(t,y,Delta,J,Theta,Tm,Td)
% QIF-FRE with exponential synapses (Eq.(3.13)).

R = y(1); V = y(2); S = y(3);

dR = (1/Tm)*(Delta/(pi*Tm) + 2*R*V);
dV = (1/Tm)*(V^2 - (pi*Tm*R)^2 - J*Tm*S + Theta);
dS = (1/Td)*(R - S);

res = [dR; dV; dS];

end
