function [t,mean_v,V,R,S,refractory_time,refractory_indexes,nSpikes] = NQIF(V0,R0,S0,refractory_time,refractory_indexes,nSpikes,Theta,J,D,Td,Tm,Ts,t0,tn,Iext,dt)
%%% Input %%%
% V0: Vector with the initial voltage for each neuron.
% R0: Initial mean firing rate.
% S0: Initial mean synaptic activity.
% refractory_indexes: Indexes from 1:N of those neurons in refractory time.
% refractory_time: Time steps left for those neurons from refractory_indexes
%                  in refractory time.
% nSpikes: Vector containing the number of spikes of the last t_window+1
%          iterations.
% Theta,J,D,Td,Tm,Ts: Parameters of the N-QIF system (see Eqs. (3.10)-(3.11))
% t0,tn: Initial and final time of integration respectively.
% Iext: External input current function.
% dt: Time step size.
% Note: If refractory_time, refractory_indexes and nSpikes are not known,
%       one cantake, for instance, refractory_time = []; 
%       refractory_indexes = []; nSpikes = zeros(1,t_window+1);
%%% Output %%%
% t: Integration time vector.
% mean_v: Mean membrane potential for each value in t.
% V: Membrane potential of each neuron in 1:N at t(end).
% R,S: Mean firing rate and synaptic activity at t(end).
% refractory_time,refractory_indexes,nSpikes: Same as input.


N = length(V0);
j = 1:N;	% Neuron indices
eta = Theta + D*tan((pi/2)*((2*j - N - 1)/(N + 1)));
Vpeak = 100; Vreset = -100;

t_window = round(Ts/dt);

V = V0;
R = R0;
S = S0;

n = round((tn-t0)/dt);
t = t0:dt:tn;

mean_v = zeros(1,n);
% s = zeros(1,n);
% r = zeros(1,n);

I = Iext(t);

i = 0;
while i < n
    mean_v(i+1) = (sum(V) - sum(V(refractory_indexes)))/(N - size(refractory_indexes,2));
%     s(i+1) = S;
%     r(i+1) = R;
    % Since some initial values can be Vpeak we do an initial iteration to
    % put those neurons in ref time
    if i > 0
        V = V + dt*(1/Tm)*((V.^2) + eta - Tm*J*S + I(i)); %Euler step
        S = S + dt*((1/Td)*(-S+R));
    end
    
    refractory_time = refractory_time - 1;                                     % The neurons in refractory state are one time step closer to getting out of it
    V(refractory_indexes) = Vreset;                                            % The neurons in refractory state cannot be updated
    Vpeak_indexes = find(V >= Vpeak);                                          % Find indexes for the neurons that have just reached Vpeak
    V(Vpeak_indexes) = Vreset;                                                 % Reset their voltage
    refractory_indexes = [refractory_indexes Vpeak_indexes];                   % Add the neurons that have just reached Vpeak into refractory state
    refractory_time(end+1:end+size(Vpeak_indexes,2)) = round(2*(Tm/Vpeak)/dt); % Initialize their counter
    finish_ref_time = find(refractory_time == 0);                              % Find the neurons that have just finished their refractory time
    refractory_indexes(finish_ref_time) = [];                                  % Remove them from the list
    refractory_time(finish_ref_time) = [];                                     % Remove them from the counter list as well
    spike_indexes = find(refractory_time == round((Tm/Vpeak)/dt));%100);       % Find the neurons that have reached +inf (the indexes are not from V!)
    nSpikes = [nSpikes,size(spike_indexes,2)];                                 % When they reach +inf, we say that they have fired a spike

    R = sum(nSpikes(end-t_window+1:end))/(N*Ts);
    i = i+1;
end

mean_v = mean_v(1:i-1);

t = t(1:i-1);

% r = r(1:i-1);
% s = s(1:i-1);
nSpikes = nSpikes(end-t_window+1:end);

end

